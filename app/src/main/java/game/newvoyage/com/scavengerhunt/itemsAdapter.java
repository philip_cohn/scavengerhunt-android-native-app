package game.newvoyage.com.scavengerhunt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import static android.R.attr.fragment;

/**
 * Created by Phil on 12/10/2017.
 */


public class itemsAdapter extends RecyclerView.Adapter<itemsAdapter.ViewHolder> {
    private ArrayList<ItemInfo> mDataset;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageButton mButton;
        public TextView mTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            mButton = (ImageButton) itemView.findViewById(R.id.item_button);
            mTextView = (TextView) itemView.findViewById(R.id.item_button_text);
          //  mButton.setOnClickListener(new View.OnClickListener() {

         //   });
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public itemsAdapter(ArrayList<ItemInfo> myDataset , Context context) {
        mDataset = myDataset;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public itemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
       View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemview, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        // uses the icon id (name) to locate the drawable thumbnail for it. The thumbnail must be named appropriately, or it won't locate it
        int resourceId = mContext.getResources().getIdentifier(mDataset.get(position).getIconId(), "drawable", "game.newvoyage.com.scavengerhunt");
       holder.mButton.setTag(mDataset.get(position).getItemName());
        holder.mButton.setImageResource(resourceId);
        holder.mButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        holder.mButton.setBackgroundColor(Color.alpha(0));
        holder.mTextView.setText(mDataset.get(position).getItemName());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}
