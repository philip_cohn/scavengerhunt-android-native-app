package game.newvoyage.com.scavengerhunt;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ItemInfo> itemsArray;

    //TODO: fix counter
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Get the id for the set of items, so we know which set of items we have to load
        Intent intent = getIntent();
        String itemsSetId = intent.getStringExtra("itemsSetId");
        Log.d("items id is " , itemsSetId);

        itemsArray = new ArrayList<ItemInfo>();
        NestedScrollView scrollLayout =(NestedScrollView) findViewById(R.id.itemlistscrollview);
        ImageView barLayout =(ImageView) findViewById(R.id.barView);
        TextView listTitle = (TextView) findViewById(R.id.toolbar_title);
        RelativeLayout titleBackground = (RelativeLayout) findViewById (R.id.title_background);



        if (itemsSetId.equals("Forest")){
            ItemInfo testItem1 = new ItemInfo("acorn");
            ItemInfo testItem2 = new ItemInfo("bee");
            ItemInfo testItem3 = new ItemInfo("bird");
            ItemInfo testItem4 = new ItemInfo("deer");
            ItemInfo testItem5 = new ItemInfo("fern");
            ItemInfo testItem6 = new ItemInfo("fungi");
            ItemInfo testItem7 = new ItemInfo("moss");
            ItemInfo testItem8 = new ItemInfo("tree");
            ItemInfo testItem9 = new ItemInfo("sapling");
            itemsArray.addAll(Arrays.asList(testItem1,testItem2,testItem3,testItem4,testItem5,testItem6,testItem7,testItem8,testItem9));
            scrollLayout.setBackgroundResource(R.drawable.forestbackground);
            barLayout.setBackgroundResource(R.drawable.forestbannerthin);
            int stringId = this.getResources().getIdentifier("forest", "string", "game.newvoyage.com.scavengerhunt");
            listTitle.setText(this.getString(stringId));
            titleBackground.setBackgroundResource(R.drawable.frame2);
            titleBackground.getBackground().setAlpha(190);
           // layout.setBackground(ContextCompat.getDrawable(this, R.drawable.forestbackground));


        }else if (itemsSetId.equals("Beach")){
            ItemInfo testItem1 = new ItemInfo("anaenomae");
            ItemInfo testItem2 = new ItemInfo("crab");
            ItemInfo testItem3 = new ItemInfo("dolphin");
            ItemInfo testItem4 = new ItemInfo("fish");
            ItemInfo testItem5 = new ItemInfo("octopus");
            ItemInfo testItem6 = new ItemInfo("oystercatcher");
            ItemInfo testItem7 = new ItemInfo("shell");
            ItemInfo testItem8 = new ItemInfo("snail");
            ItemInfo testItem9 = new ItemInfo("starfish");
            itemsArray.addAll(Arrays.asList(testItem1,testItem2,testItem3,testItem4,testItem5,testItem6,testItem7,testItem8,testItem9));
            scrollLayout.setBackgroundResource(R.drawable.seabackground);
            barLayout.setBackgroundResource(R.drawable.seabannerthin);
            int stringId = this.getResources().getIdentifier("beach", "string", "game.newvoyage.com.scavengerhunt");
            listTitle.setText(this.getString(stringId));
            titleBackground.setBackgroundResource(R.drawable.frame1);
            titleBackground.getBackground().setAlpha(190);
        }



        mRecyclerView = (RecyclerView) findViewById(R.id.myrecyclerview);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new itemsAdapter(itemsArray, this);
        mRecyclerView.setAdapter(mAdapter);


    }

    public void goToInfo(View v) {
        Intent intent = new Intent(v.getContext(), InformationActivity.class);
        intent.putExtra("itemName", (String)v.getTag());
        v.getContext().startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();

    }
}
