package game.newvoyage.com.scavengerhunt;

import android.graphics.Bitmap;

/**
 * Created by Phil on 15/10/2017.
 */

public class ItemInfo {


    public String itemName;
    public String itemDescription;
    public String iconId;
    public String userPhotoId;

    public ItemInfo(String itemName) {
        this.itemName = itemName;
        this.itemDescription = itemName+"_description";
        this.iconId = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getIconId() {
        return iconId;
    }

    public String getUserPhotoId() {
        return userPhotoId;
    }

    public void setUserPhotoId(String userPhotoId) {
        this.userPhotoId = userPhotoId;
    }



}
